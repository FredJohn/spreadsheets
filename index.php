<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Fred Duarte">



        <link rel="icon" href="favicon.ico">

        <title>Google Docs</title>

        <link rel="stylesheet" href="css/styles.css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript" src="js/spin.js"></script>
        <script type="text/javascript" src="js/bootbox.js"></script>
        <script type="text/javascript" src="https://apis.google.com/js/client.js"></script>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Google Docs</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">
            <div class="starter-template">
                <div class="notification"></div>
                <h1>Addresses</h1>

                <div class="address_list">
                    <div class="table-responsive">
                        <table class="table table-striped hidden">
                            <thead>
                                <tr>
                                    <th>Naam</th>
                                    <th>Email</th>
                                    <th>Adres</th>
                                    <th>Postcode</th>
                                    <th>Plaats</th>
                                </tr>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>
                    </div>
                    <div id="address_add" data-modal-id="modal_address_add" class="btn btn-primary modal-trigger">Add new</div>
                    <div id="export" class="btn btn-primary">Export</div>

                </div>

            </div>


            <!-- Modal -->
            <div class="modal fade" id="modal_address_add" tabindex="-1"  role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Add new address</h4>
                        </div>
                        <form role="form" id="new_address">
                            <div class="modal-body">

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="form-label" for="uname">Name</label>
                                    <div class="controls">
                                        <input id="uname" class="form-control input-xxlarge" name="uname" placeholder="Name" required="" type="text">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="form-label" for="email">Email</label>
                                    <div class="controls">
                                        <input id="email" class="form-control input-xxlarge" name="email" placeholder="Email address" required="" type="email">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="form-label" for="address">Address</label>
                                    <div class="controls">
                                        <input id="address" class="form-control input-xxlarge" name="address" placeholder="Address" required="" type="text">
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- Text input-->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label" for="postalcode">Postal code</label>
                                            <div class="controls">
                                                <input id="postalcode" class="form-control input-xxlarge" name="postalcode" placeholder="Postal code" required="" type="text">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label" for="city">City</label>
                                            <div class="controls">
                                                <input id="city" class="form-control input-xxlarge" name="city" placeholder="City" required="" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" id="add-address" class="btn btn-success">Add another</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Finish</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>


            <!-- Modal -->
            <div class="modal fade" id="modal_download" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" >Download your file.</h4>
                        </div>
                        <div class="modal-body text-center">
                            <h2>Success <span class="glyphicon text-success glyphicon-ok" aria-hidden="true"></span></h2>
                            <div class="status-message"></div>
                            <div id="download-links"></div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container -->

    </body>
</html>
