<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ajax
 *
 * @author Fred
 */
class Ajax {

    protected $request;
    protected $action;
    protected $url;
    protected $data;
    protected $token;
    protected $ch;
    protected $headers;

    public function __construct($request) {

        //echo "Initializing ajax class";
        $this->request = $request;
        $this->action = $request['action'];
        $this->url = $request['url'];
        $this->data = json_decode($request['data']);
        $this->token = $request['token'];

        $this->headers = array(
            'Gdata-version: 3.0',
            'Content-type: application/atom+xml',
            'Authorization: Bearer ' . $this->token
        );
        $this->checkAction($this->action);
    }

    /**
     * Checks if it's a valid ajax call and run it
     * @param string $action The action name
     */
    public function checkAction($action) {

        // List of allowed calls
        $allowedCalls = array(
            'add_headers' => "addHeaders",
            'add_rows' => "addRows"
        );

        // If it's allowed lets run it
        if (isset($allowedCalls[$action])) {
            //echo "Call Allowed";
            $method = $allowedCalls[$action];
            $this->$method();
        } else {
            $this->response("error", "Invalid ajax call");
        }
    }

    /**
     * Adds the title headers to the worksheet
     */
    public function addHeaders() {

        $this->curl_start($this->url, $this->data, "POST");

        $this->headers[] = 'If-match: *';

        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->data);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        $this->curl_run();
    }

    /**
     * Adds Rows to the spreadsheet as long as there are headers
     * Unfortunately the API does not support batch insert of rows just of cells.
     * So we need to make a curl call for each row...
     */
    public function addRows() {

        $this->ch = curl_init($this->url);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($this->ch, CURLOPT_POST, 1);

        // Keep track of how many were and were not inserted
        $inserted = 0;
        $failed = 0;

        foreach ($this->data as $row) {
            $response = false;
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $row);
            $response = curl_exec($this->ch);

            if ($response === false) {
                $failed++;
            } else {
                $inserted++;
            }
        }

        // Either output an error or success message depending on success to fail ratio
        $status = $inserted > $failed ? 'success' : 'error';
        
        // Error
        if ($failed >= $inserted) {
            $error = curl_error($this->ch);
            $this->response($status, "There has been an error. $error . <br />Addresses saved: $inserted | failed: $failed. ");
        } else {
            $link_excel = $this->request['file_link'];
            $link_pdf   = $this->request['file_pdf'];
            $this->response($status, "Your file has been exported! Fields inserted: $inserted | failed: $failed.", array('excel' => $link_excel, 'pdf' => $link_pdf));
        }
        
        curl_close($this->ch);
    }

    public function curl_start($url, $data, $type = "POST") {

        $this->ch = curl_init($url);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);

        if ($type === "POST") {
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        }
    }

    public function curl_run() {

        $response = curl_exec($this->ch);

        if (!$response) {
            $this->response('error', "There has been an error submitting the data to Google API");
            //echo 'Curl error: ' . curl_error($this->ch);
        } else {
            echo $response;
        }
    }

    /**
     * Sends back an ajax response
     * @param string $status 'success' if execution of ajax action has succeeded. 'error' if action failed
     * @param string $message A success or error message
     * @param array $extra Any other information or objects that you need to pass
     */
    public function response($status, $message, $extra = NULL) {
        $status = $status == 'error' ? 'danger' : $status;
        $response = array(
            'status' => $status,
            'message' => $message
        );

        if ($extra != NULL) {
            $response['extra'] = $extra;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        return;
    }

}
