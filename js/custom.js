jQuery(document).ready(function () {

    // Lets store emails here to check for duplicates
    var emails = [];


    // Handle clicking on the add address button
    jQuery("#address_add").click(function (e) {
        e.preventDefault();

        var modal_id = "#" + jQuery(this).data('modal-id');
        jQuery(modal_id).modal("show");
    });


    // Handle for submit
    jQuery("#new_address").submit(function (e) {
        console.log(emails);
        e.preventDefault();

        var email = jQuery("#email").val();

        // Table entry
        var html = '<tr>';
        html += '<td>' + jQuery("#uname").val() + '</td>';
        html += '<td>' + email + '</td>';
        html += '<td>' + jQuery("#address").val() + '</td>';
        html += '<td>' + jQuery("#postalcode").val() + '</td>';
        html += '<td>' + jQuery("#city").val() + '</td>';
        html += '</tr>';

        // If email already exists
        if (emails.indexOf(email) > -1) {
            bootbox.alert("An entry with that email already exists!");
        } else {
            
            // Add email to array and insert data into table
            emails.push(email);
            jQuery(".address_list tbody").append(html);
            jQuery("table").removeClass('hidden');
        }
    });

    // Handle export button click
    jQuery("#export").click(function (e) {

        e.preventDefault();

        // Stop if there are no addresses
        var address_count = jQuery("tbody tr").length;
        if (address_count < 1) {
            bootbox.alert("You need at least one address to be able to export.");
            return;
        }

        // Lets reset any spreadsheet name that might exist
        SPREADSHEET_FILENAME = null;
        
        // Ask for filename
        bootbox.prompt("Please choose a title for your spreadsheet file.", function (result) {
            
            if (result === null) {
                console.log("Filename prompt dismissed.");
                
            } else {
                
                // Show loading spin
                jQuery('html').spin('large');
                
                // We need to append this because closing the oauth window will not trigger a callback
                jQuery('.spinner').append('<a href="#"><span id="stop">Cancel login?</span></a>');
                SPREADSHEET_FILENAME = result;

                handleClientLoad();
            }
        });

    });

    // Dismissing the oauth screen by closing the window will not trigger any callback
    // so we don't know if the user canceled the login by closing the window.
    // oAuth only calls our callback if the user clicks the Refuse or Authorize button,
    // but many might just close the window, so we just add a "Cancel login" link
    jQuery("html").on("click", '#stop', function (e) {
        e.preventDefault();
        ajax_error("Please authenticate with Google to export the file.");
    });

});

// Some needed vars
var spinner;
var SPREADSHEET_FILENAME;
var SPREADSHEET_URL;
var SPREADSHEET_PDF;
var SPREADSHEET_CSV;
var TOKEN;
var CLIENT_ID = '452875086622-g5bmlq5q4b9nv7nu98535ae6j8b78n9p.apps.googleusercontent.com';
var SCOPES = [
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.readonly.metadata',
    'https://spreadsheets.google.com/feeds',
    'email',
    'profile'
];


// Function called when Google Drive API is loaded
function handleClientLoad() {
    window.setTimeout(checkAuth, 1);

}



/**
 * Check if the current user has authorized the application.
 */
function checkAuth() {

    gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES.join(' '), 'immediate': false}, handleAuthResult);

}


/**
 * Called when authorization server replies.
 *
 * @param {Object} authResult Authorization result.
 */
function handleAuthResult(authResult) {

    console.log("Auth result:");

    // If we got to here then we know the callback has been called so we can
    // remove our "cancel login" temporary link under the loading spin.
    jQuery("#stop").remove();
    
    
    console.log(authResult);
    if (authResult.error) {
        console.log("Not Logged in");
        ajax_error("Please authenticate with Google.");
        //jQuery(".modal#authenticate").modal('show');

    } else {
        console.log("Logged in");
        //console.log(authResult);
        TOKEN = authResult.access_token;
        console.log("Retrieved token: " + TOKEN);

        load_drive_api(drive_load_callback);
    }
}


/**
 * Load the Drive API client.
 * @param {Function} callback Function to call when the client is loaded.
 */
function load_drive_api(callback) {
    gapi.client.load('drive', 'v2', callback);
}

/**
 * Callback after loading Google Drive API
 */
function drive_load_callback(response) {
    drive_create_sheet();
}

function drive_create_sheet() {
    // Create request
    var request = gapi.client.drive.files.insert({
        "title": SPREADSHEET_FILENAME,
        'mimeType': "application/vnd.google-apps.spreadsheet"
    });

    // Execute request
    request.execute(function (resp) {

        if (!resp.error) {
            console.log("File created:");
            console.log(resp);
            var file_id = resp.id;

            SPREADSHEET_URL = resp.alternateLink;
            SPREADSHEET_XLS = "https://docs.google.com/spreadsheets/export?id=" + file_id + "&exportFormat=xlsx"
            SPREADSHEET_PDF = "https://docs.google.com/spreadsheets/export?id=" + file_id + "&exportFormat=pdf";
            SPREADSHEET_CSV = "https://docs.google.com/spreadsheets/export?id=" + file_id + "&exportFormat=csv";
            console.log("Spreadsheet created with ID " + file_id);
            list_spreadsheet_files();

        } else if (resp.error.code == 401) {
            // Access token might have expired.
            checkAuth();
        } else {
            ajax_error("Unable to create a file on Google Drive.");
            console.log('An error occured: ' + resp.error.message);
        }
    });
}


// Create testsheet spreadsheet file in google drive
function drive_create_testsheet() {

    // Create request
    var request = gapi.client.drive.files.insert({
        "title": SPREADSHEET_FILENAME,
        'mimeType': "application/vnd.google-apps.spreadsheet"
    });

    // Execute request
    request.execute(function (resp) {

        if (!resp.error) {
            var file_id = resp.id;
            console.log("Spreadsheet created with ID " + file_id);
            list_spreadsheet_files();

        } else if (resp.error.code == 401) {
            // Access token might have expired.
            checkAuth();
        } else {
            ajax_error("Unable to create a file on Google Drive.");
            console.log('An error occured: ' + resp.error.message);
        }
    });
}

// Retrieves a list of spreadsheet files. If a spreadsheet with filename "testsheet"
// does not exist we will create one.
function list_spreadsheet_files() {

    console.log("Listing spreadsheet files");
    var url = "https://spreadsheets.google.com/feeds/spreadsheets/private/full?alt=json";

    jQuery.ajax({
        url: url,
        headers: {
            "Authorization": "Bearer " + TOKEN
        },
        success: function (response) {

            var results = response.feed.entry;
            console.log("Spreadsheets:");
            console.log(results);

            // Loop through each entry

            var file_found = false;

            jQuery(results).each(function () {

                // Let's find our testsheet
                if (this.title.$t === SPREADSHEET_FILENAME) {
                    console.log("SPREADSHEET TITLE IS " + this.title.$t);
                    console.log("Test spreadsheet found with ID " + this.id.$t);
                    file_found = true;

                    links = this.link;

                    var worksheet_feed = get_link(links, "http://schemas.google.com/spreadsheets/2006#worksheetsfeed");
                    list_worksheets(worksheet_feed);
                    return;

                }

            });

//            if (!file_found) {
//                // If no file found create one
//                console.log("Test spreadsheet does not exist. Creating...");
//                drive_create_testsheet();
//            }

        },
        error: function (error) {
            ajax_error("There has been an error listing spreadsheet files.");
            console.log(error);
        }
    });

}

function list_worksheets(worksheet) {

    console.log("Retrieving worksheets...");
    var url = worksheet + '?alt=json';

    jQuery.ajax({
        url: url,
        headers: {
            "Authorization": "Bearer " + TOKEN
        },
        success: function (response) {

            console.log("Worksheets feed:");
            console.log(response);
            var links = response.feed.entry[0].link;
            var postfeed_links = response.feed.link;

            cellfeed_url = get_link(links, "http://schemas.google.com/spreadsheets/2006#cellsfeed");
            worksheet_url = response.feed.id.$t;
            postfeed_url = get_link(links, "http://schemas.google.com/spreadsheets/2006#listfeed");
            console.log("POST FEED IS " + postfeed_url);
            //list_worksheet_cell(cell_url);
            add_worksheet_headers(worksheet_url, cellfeed_url, postfeed_url);

            // Lists all data in the worksheet as long as its not empty.
            //data_url = get_link(links, "http://schemas.google.com/spreadsheets/2006#listfeed");
            //list_worksheet_data(data_url);

        },
        error: function (response) {
            ajax_error("There has been an error retrieving worksheets list from file.");
        }
    });
}


function add_worksheet_headers(worksheet_url, cellfeed_url, postfeed_url) {


    console.log("Fetching XML batch template");


    // Get template first
    jQuery.ajax({
        url: 'templates/worksheet_headers.xml',
        dataType: "text",
        success: function (response) {
            add_worksheet_headers_callback(worksheet_url, cellfeed_url, postfeed_url, response);
        },
        error: function () {
            ajax_error("Error fetching headers to insert on worksheet.");
        }
    });

}

function add_worksheet_headers_callback(worksheet_url, cellfeed_url, postfeed_url, xml) {


    xml = xml.replace("WORKSHEET_URL", worksheet_url);

    xml = xml.replace(new RegExp("CELLFEED_URL", "gm"), cellfeed_url);

    console.log("EDITED XML IS:");
    console.log(xml);
    console.log("Sending to proxy..");

    jQuery.ajax({
        url: 'ajax-handler.php',
        type: "POST",
        data: {
            action: 'add_headers',
            token: TOKEN,
            url: cellfeed_url + '/batch',
            data: JSON.stringify(xml)
        },
        success: function (response) {
            console.log("Add headers:");
            console.log(response);
            //add_sample_data(postfeed_url);
            add_data(postfeed_url);
        },
        error: function () {
            ajax_error("Error adding headers to worksheet.");
        }
    });

}

function add_data(url) {

    console.log("Adding data..");
    var row;

    // Create empty array to store addresses
    var addresses = [];

    // Loop through the table rows
    var table_rows = jQuery("tbody tr");

    jQuery(table_rows).each(function (i, el) {

        var name = jQuery(el).children("td:eq(0)").text();
        var email = jQuery(el).children("td:eq(1)").text();
        var address = jQuery(el).children("td:eq(2)").text();

        var housenr = '';
        var affix = '';

        // Let's split the address
        var regex = /^(\d*[\wäöüß\d '\-\.]+)[,\s]+(\d+)\s*([\wäöüß\d\-\/]*)$/i;
        var splitted = address.match(regex);

        if (splitted && splitted.length > 2) {
            splitted.shift();

            address = splitted[0];
            housenr = splitted[1];
            affix = splitted[2];
        }

        var postalcode = jQuery(el).children("td:eq(3)").text();
        var city = jQuery(el).children("td:eq(4)").text();

        // Create object
        var entry = {
            name: name,
            email: email,
            address: address,
            housenr: housenr,
            affix: affix,
            postal: postalcode,
            city: city
        };

        // Add to addresses array
        addresses.push(entry);
    });

    // Get template first
    jQuery.ajax({
        url: 'templates/row.xml',
        dataType: "text",
        success: function (response) {

            row = response;
            add_data_callback(url, addresses, row);


        },
        error: function () {
            ajax_error("Error fetching row xml template.");
        }
    });

}
function add_sample_data(url) {

    console.log("Adding sample data..");
    var row;
    addresses = [
        {
            name: "John",
            email: "john@gmail.com",
            address: "roadstreet",
            housenr: "22",
            affix: 'A',
            postal: "4444AA",
            city: "London"
        },
        {
            name: "Carl",
            email: "carl@gmail.com",
            address: "carl road",
            housenr: "643",
            affix: 'F',
            postal: "435CJB",
            city: "Amsterdam"
        },
        {
            name: "Fred",
            email: "fred@gmail.com",
            address: "Horstraat",
            housenr: "11",
            affix: '',
            postal: "5462AJ",
            city: "Amsterdam"
        },
        {
            name: "Pedro",
            email: "pedro@gmail.com",
            address: "pedro road",
            housenr: "643",
            affix: '',
            postal: "435CJB",
            city: "Amsterdam"
        }
    ];

    // Get template first
    jQuery.ajax({
        url: 'templates/row.xml',
        dataType: "text",
        success: function (response) {

            row = response;
            add_sample_data_callback(url, addresses, row);

        },
        error: function () {
            ajax_error("Error adding sample data.");
        }
    });
}

function add_data_callback(url, addresses, row_template) {

    // Header is set apart so that we can loop through multiple entries
    var rows = [];

    jQuery.each(addresses, function () {
        var this_row = row_template;
        this_row = this_row.replace('NAME', this.name);
        this_row = this_row.replace('EMAIL', this.email);
        this_row = this_row.replace('ADDRESS', this.address);
        this_row = this_row.replace('HOUSE', this.housenr);
        this_row = this_row.replace('AFFIX', this.affix);
        this_row = this_row.replace('POSTCODE', this.postal);
        this_row = this_row.replace('CITY', this.city);
        rows.push(this_row);
    });

    console.log("Rows to upload is: ");
    console.log(rows);

    console.log("Sending to proxy..");

    jQuery.ajax({
        url: 'ajax-handler.php',
        type: "POST",
        data: {
            action: 'add_rows',
            token: TOKEN,
            url: url,
            data: JSON.stringify(rows)
        },
        success: function (response) {

            if (response.status === 'success') {
                jQuery('html').spin(false);
                jQuery("#download-links").html('');
                jQuery("#modal_download .status-message").text(response.message);

                jQuery("<a>").attr({
                    'href': SPREADSHEET_URL,
                    'target': "_blank"
                }).html('<img title="View on Google Docs" src="img/drive.png"/>').appendTo("#download-links");

                jQuery("<a>").attr('href', SPREADSHEET_XLS).html('<img title="Download as XLS" src="img/xls.png"/>').appendTo("#download-links");
                jQuery("<a>").attr('href', SPREADSHEET_CSV).html('<img title="Download as CSV" src="img/csv.png"/>').appendTo("#download-links");
                jQuery("<a>").attr('href', SPREADSHEET_PDF).html('<img title="Download as PDF" src="img/pdf.png"/>').appendTo("#download-links");

                jQuery("#modal_download").modal("show");
            } else {
                ajax_error(response.message);
            }
        },
        error: function () {
            ajax_error("Error uploading to server.");
        }
    });



}


/**
 * 
 * Below functions are for a single cell
 * 
 */

function list_worksheet_cell(url) {

    console.log("Retrieving cell");
    url += '/R1C2?alt=json';

    jQuery.ajax({
        url: url,
        headers: {
            "Authorization": "Bearer " + TOKEN,
        },
        success: function (response) {

            console.log("Cell result:");
            console.log(response);

            var cell = response.entry;
            update_worksheet_cell(cell);


        },
        error: function (response) {
            console.log("Error retrieving cell:");
            console.log(response);
        }
    });

}

function update_worksheet_cell(cell) {

    console.log("Fetching XML cell template");


    // Get template first
    jQuery.ajax({
        url: 'templates/cell_template.xml',
        dataType: "text",
        success: function (response) {

            update_worksheet_cell_callback(response, cell);
        }
    });



}

function update_worksheet_cell_callback(xml_cell, cell) {

    var new_value = "wooop!";

    var cell_url = cell.id.$t;
    var row = cell.gs$cell.row;
    var col = cell.gs$cell.col;

    var links = cell.link;
    var edit_url = get_link(links, 'edit');

    var xml = xml_cell.replace("CELL_ID", cell_url);
    var xml = xml.replace("CELL_EDIT_LINK", edit_url);
    var xml = xml.replace("CELL_ROW", row);
    var xml = xml.replace("CELL_COL", col);
    var xml = xml.replace("CELL_VALUE", new_value);

    console.log("Cell details:");
    console.log(cell);
    console.log("Our filled in XML:");
    console.log(xml);


    /**
     * Unfortunately Google APi does not respond correctly to CORS headers
     * Seems I'm not the only one with this issue
     * Link: http://clchiou.github.io/2014-07-07/google-spreadsheet-apis-support-of-cors/
     * Link: https://groups.google.com/forum/#!searchin/google-spreadsheets-api/cors$20preflight/google-spreadsheets-api/f8LLOa5JpiU/vuyreJ5BeGUJ
     * Link: http://stackoverflow.com/questions/20169829/cors-preflight-fails-when-writing-to-google-spreadsheet-api.
     * 
     * The only solution is to use a server side call from our server..
     */

    console.log("Sending to proxy..");

    jQuery.ajax({
        url: 'proxy.php',
        type: "POST",
        data: {
            token: TOKEN,
            cell_url: cell_url,
            data: JSON.stringify(xml)
        },
        success: function (response) {
            console.log(response);
        },
        error: function () {
            console.log("Error");
        }
    });

//    jQuery.ajax({
//        url: cell_url,
//        data: gs_cell_sample,
//        crossDomain: true,
//        type: "PUT",
//        headers: {
//            "Authorization": "Bearer " + TOKEN,
//            "If-Match": "*",
//            "Content-type": "application/atom+xml"
//        },
//        success: function (response) {
//            console.log(response);
//        },
//        error: function (error) {
//            console.log("Error updating cell:");
//            console.log(error);
//        }
//    });
}


// END SINGLE CELL FUNCTIONS





function list_worksheet_data(url) {

    url += "?alt=json";

    jQuery.ajax({
        url: url,
        headers: {
            "Authorization": "Bearer " + TOKEN
        },
        success: function (response) {

            console.log("Showing data in worksheet..");
            console.log(response);
        },
        error: function () {
            console.log("Error");
        }
    });
}



/**
 * Helper function that returns the URL given a collection of links and a rel 
 * attribute that we are looking for
 * @param links jQuery object links from google api result (feed.entry[0].links)
 * @param rel str rel Ex: http://schemas.google.com/spreadsheets/2006
 * @returns str The URL we are looking for the REL given.
 */
function get_link(links, rel) {

    var url;

    jQuery(links).each(function () {

        if (this.rel === rel) {
            url = this.href;
            return;
        }
    });

    return url;

}


// Saving time
function c(my_var) {
    console.log(my_var);
}

function ajax_error(error) {

    jQuery("html").spin(false);
    show_notification('.notification', 'danger', error, true);
}
/*
 * Shows a bootstrap panel alert
 * @var selector    obj         Existing DOM element. The alert will be inserted after this element
 * @var type        string      The type of message. It can be success, warning, info, danger
 * @var message     string      The message that will be displayed in the alert
 * @var dismissable boolean     Can the panel be dismissed? If yes a "closing x" will apear on the top right
 */
function show_notification(selector, type, message, dismissable) {

    jQuery(selector + ' .alert').remove();
    var alert = jQuery('<div></div<').attr({
        class: "alert alert-" + type,
        role: "alert"
    });

    if (dismissable === true) {
        jQuery(alert).append('<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span>\n\
        <span class="sr-only">Close</span></button>');
    }

    jQuery(alert).append(message);

    jQuery(selector).hide().append(alert).fadeIn('slow', function () {
    });

}